# arg - A simple command line argument parser
This is a simple tool with a simple job: figure out the value of a single
command line argument. Tools like `getopt` and `getopts` are unnecessarily
complex and difficult to use, while also not supporting common use cases well.
Parsing an argument in a shell script shouldn't require a tutorial or dozen
lines of shell code!

To quote the `getopt` manpage:

> Each shellscript has to carry complex code to parse arguments halfway
> correcty (like the example presented here). A better getopt-like tool would
> move much of the complexity into the tool and keep the client shell scripts
> simpler.

## Usage
Simply run: `arg <flag> <extra args...>`.  `<flag>` can be anything, like `-f`,
`-flag`, `--flag`, or `flag`.  If the flag occurs among the rest of the
command line arguments, `arg` will print the flag's value (if any) and exit
successfully, otherwise, it will fail (exit status 1). In a shell script, this
would look like:

```
#!/bin/sh
dir=`arg --dir "$@" || arg -d "$@" || echo "$HOME/Downloads"`
if ! server=`arg --server "$@" || arg -s "$@"`; then
    echo "No server provided :(" && exit 1
fi
if arg --verbose "$@" || arg -v "$@"; then
    echo "Downloading to $dir"
fi
curl "$server/file.zip" > "$dir/file.zip"
```

The flag's value may be of the form `<flag>=<value>` or `<flag> <value>`.
Single-letter flags will also match when grouped with other single letter
flags, but will not have a value (e.g. `arg -b -abc foo` will succeed without
printing anything).

When using `arg` in a shell script, it is best to use quotes around `$@`, as in
`arg --foo "$@"`, so arguments with spaces will be forwarded correctly.
